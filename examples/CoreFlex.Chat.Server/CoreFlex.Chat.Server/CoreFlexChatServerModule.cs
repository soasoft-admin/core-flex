﻿using CoreFlex.Chat.Server.Client;
using CoreFlex.Module;

namespace CoreFlex.Chat.Server;

[DependOns(typeof(CoreFlexChatServerClientModule))]
public class CoreFlexChatServerModule : CoreFlexModule
{
    
}
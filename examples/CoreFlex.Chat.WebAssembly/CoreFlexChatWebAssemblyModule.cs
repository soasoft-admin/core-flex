﻿using CoreFlex.Chat.Rcl;
using CoreFlex.Module;

namespace CoreFlex.Chat.WebAssembly;

[DependOns(typeof(CoreFlexChatRclModule))]
public class CoreFlexChatWebAssemblyModule : CoreFlexModule
{

}